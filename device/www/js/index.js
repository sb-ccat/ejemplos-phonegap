console.log('iniciando...');

$(document).on('pageinit', '#indexPage', indexPageIniciado);

function indexPageIniciado() {
    console.log('se inicio indexPage');
    document.addEventListener('deviceready', onDeviceReady, false);
}

function onDeviceReady() {
    console.log('device is ready');

	loadDeviceData();

    $('#exit').click(exit);
}

function exit() {
	navigator.app.exitApp();
}