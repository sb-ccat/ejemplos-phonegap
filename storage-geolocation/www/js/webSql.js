$(document).on('pageinit', '#webSqlPage', webSqlPageIniciado);

function handleError(e) {
    alert('ocurrió un error, revise la consola');
    console.log('ocurrió un error', e)
}

function webSqlPageIniciado() {
    var dbCreada = localStorage.getItem('dbCreada');

    if (!dbCreada) {
        createTableLibro(handleError, function() {
            createTableVenta(handleError, function() {
                /*alert('BD creada correctamente');*/
                createTableMoneda(handleError, function(){
                    insertarPrimerLibro(handleError, function() {
                        insertarMasLibros(handleError, function() {
                            insertarPrimerVenta(handleError, function() {
                                insertarMonedas(handleError, function() {
                                    alert('datos insertados correctamente');
                                    localStorage.setItem('dbCreada','1');
                                    iniciarUI();
                                });
                            });
                        });
                    });
                });
            });
        });
    } else {
        iniciarUI();
    }
}

function iniciarUI() {
    $('#saGuardar').on('tap', function(event) {
        guardar();
    });

    loadLibrosToSelect();
    displayLibros();
    displayVentas();
}

function insertarMonedas(errorCB, successCB) {
    insertMoneda('Bolivianos', errorCB, function() {
        insertMoneda('Dolares', errorCB, successCB);
    });
}

function insertarPrimerLibro(errorCB, successCB) {
    insertLibro('El Hobbit', '200', errorCB, successCB);
}

function insertarMasLibros(errorCB, successCB) {
    var libros = [
        { name: 'El Señor de los Anillos', price: '600' },
        { name: 'El Silmarillion', price: '400' }
    ];
    insertLibros(libros, errorCB, successCB);
}

function insertarPrimerVenta(errorCB, successCB) {
    insertVenta('1','Miguel Ordoñez', '14/05/2014', 0, 0, errorCB, successCB);
}

function getDatabase() {
    return window.openDatabase('librosDB', '1', 'Libros', 200*1024);
}

function executeSql(sqlSentence, params, errorCB, successCB) {
    getDatabase().transaction(function(tx) {
        tx.executeSql(sqlSentence, params);
    }, errorCB, successCB);
}

function executeBatch(sqlSentences, errorCB, successCB) {
    getDatabase().transaction(function(tx) {
        for (var x in sqlSentences) {
            tx.executeSql(sqlSentences[x].command, sqlSentences[x].params);
        }
    }, errorCB, successCB);
}

function executeSelect(sqlSentence, params, errorCB, successCB) {
    getDatabase().transaction(function(tx) {
        tx.executeSql(sqlSentence, params,
            function(tx, result) {
                successCB(tx, result);
            }, errorCB);
    });
}

function createTableLibro(errorCB, successCB) {
    var sqlSentence = 'CREATE TABLE IF NOT EXISTS LIBRO (ID INTEGER PRIMARY KEY AUTOINCREMENT,'
    + ' NOMBRE, PRECIO)';
    executeSql(sqlSentence, [], errorCB, successCB);
}

function createTableVenta(errorCB, successCB) {
    var sqlSentence = 'CREATE TABLE IF NOT EXISTS VENTA (ID INTEGER PRIMARY KEY AUTOINCREMENT,'
    + ' ID_LIBRO, CONTACTO, FECHA, LONGITUD, LATITUD)';
    executeSql(sqlSentence, [], errorCB, successCB);
}

function createTableMoneda(errorCB, successCB) {
    var sqlSentence = 'CREATE TABLE IF NOT EXISTS MONEDA (ID INTEGER PRIMARY KEY AUTOINCREMENT,'
        + ' NOMBRE)';
    executeSql(sqlSentence, [], errorCB, successCB);
}

function insertMoneda(nombre, errorCB, successCB) {
    var sqlSentence = 'INSERT INTO MONEDA (NOMBRE) VALUES (?)';
    var params = [nombre];
    executeSql(sqlSentence, params, errorCB, successCB);
}

function insertLibro(nombre, precio, errorCB, successCB) {
    var sqlSentence = 'INSERT INTO LIBRO (NOMBRE, PRECIO) VALUES (?, ?)';
    var params = [nombre, precio];
    executeSql(sqlSentence, params, errorCB, successCB);
}

function insertLibros(libros, errorCB, successCB) {
    var sentences = [];
    for (var x in libros) {
        var sentence = {};
        sentence.command = 'INSERT INTO LIBRO (NOMBRE, PRECIO) VALUES (?, ?)';
        sentence.params = [libros[x].name, libros[x].price];
        sentences.push(sentence);
    }
    executeBatch(sentences, errorCB, successCB);
}

var coordLat, coordLong;   

function monitorearPosicion() {
    navigator.geolocation.watchPosition(function (position) {
        coordLat = position.coords.latitude;
        coordLong = position.coords.longitude;
        console.log('gps capturado');
    }, function(){
        console.log('hubo un error con el gps');
    }, 
    { maximumAge: 60000, timeout: 30000, enableHighAccuracy: true });
} 

function insertVenta(id_libro, contacto, fecha, longitud, latitud, errorCB, successCB) {
    var sqlSentence = 'INSERT INTO VENTA (ID_LIBRO, CONTACTO, FECHA, LONGITUD, LATITUD)'
    + ' VALUES (?,?,?,?,?)';

    var params = [id_libro, contacto, fecha, coordLong, coordLat];
    executeSql(sqlSentence, params, errorCB, successCB);
}

function getTableLibro(errorCB, successCB) {
    var sqlSentence = 'SELECT * FROM LIBRO';
    executeSelect(sqlSentence, [], errorCB, successCB);
}

this.getTableVenta = function(errorCB, successCB) {
    var sqlSentence = 'SELECT * FROM VENTA';
    executeSelect(sqlSentence, [], errorCB, successCB); //successCB(tx, SQLResultSet)
}
/////////////////////////////////////////////////////////////////////////////////////////////

function loadLibrosToSelect() {
    //fetch libros
    getTableLibro(handleError, loadLibrosToSelectCB);
}

function loadLibrosToSelectCB(tx, results) {
    //loop libros

    for (var i=0; i<results.rows.length; i++) {
        //create option
        var option = $('<option />', {
            'value' : results.rows.item(i).ID,
            text : results.rows.item(i).NOMBRE
        });

        //append to grid
        $('#saLibro').append(option);
    }
}

function displayLibros() {
    //reset grid
    $('#saLibros').children().remove();

    var divId = $('<div />', {
        'class' : 'ui-block-a',
        text : 'ID - NOMBRE'
    });

    var divPrecio = $('<div />', {
        'class' : 'ui-block-b',
        text : 'PRECIO'
    });

    $('#saLibros').append(divId);
    $('#saLibros').append(divPrecio);

    //fetch libros
    getTableLibro(handleError, displayLibrosCB);
}

function displayLibrosCB(tx, results) {
    //loop libros
//        alert(JSON.stringify(results.rows.item(0)));
    for (var i=0; i<results.rows.length; i++) {
        //create div per property
        var divId = $('<div />', {
            'class' : 'ui-block-a',
            text : results.rows.item(i).ID + " - " + results.rows.item(i).NOMBRE
        });

        var divPrecio = $('<div />', {
            'class' : 'ui-block-b',
            text : results.rows.item(i).PRECIO
        });

        //append to grid
        $('#saLibros').append(divId);
        $('#saLibros').append(divPrecio);
    }
}

function displayVentas() {
    //reset grid
    $('#saVentas').children().remove();

    var divId = $('<div />', {
        'class' : 'ui-block-a',
        text : 'ID - ID_LIBRO'
    });

    var divContacto = $('<div />', {
        'class' : 'ui-block-b',
        text : 'CONTACTO'
    });

    var divLongitud = $('<div />', {
        'class' : 'ui-block-c',
        text : 'LONGITUD'
    });

    var divLatitud = $('<div />', {
        'class' : 'ui-block-d',
        text : 'LATITUD'
    });

    //append to grid
    $('#saVentas').append(divId);
    $('#saVentas').append(divContacto);
    $('#saVentas').append(divLongitud);
    $('#saVentas').append(divLatitud);

    //fetch libros
    getTableVenta(handleError, displayVentasCB);

}

function displayVentasCB(tx, results) {
    //loop ventas
//        alert(JSON.stringify(results.rows.item(0)));
    for (var i=0; i<results.rows.length; i++) {
        //create div per property
        var divId = $('<div />', {
            'class' : 'ui-block-a',
            text : results.rows.item(i).ID + " - " + results.rows.item(i).ID_LIBRO
        });

        var divContacto = $('<div />', {
            'class' : 'ui-block-b',
            text : results.rows.item(i).CONTACTO
        });

        var divLongitud = $('<div />', {
            'class' : 'ui-block-c',
            text : results.rows.item(i).LONGITUD
        });

        var divLatitud = $('<div />', {
            'class' : 'ui-block-d',
            text : results.rows.item(i).LATITUD
        });

        //append to grid
        $('#saVentas').append(divId);
        $('#saVentas').append(divContacto);
        $('#saVentas').append(divLongitud);
        $('#saVentas').append(divLatitud);
    }

}
function guardar() {
    var today = new Date();

    insertVenta($('#saLibro').val(), $('#saContacto').val(), today.toString(), 0, 0, handleError, displayVentas);
}
