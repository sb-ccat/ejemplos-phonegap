document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    console.log('app iniciada');

    initDatabase();

    $('#save').click(save);
}

////////////////////////////////////////////////////////////////////////////////

function save() {
    var equipoA = $('#equipoA').val();
    var equipoB = $('#equipoB').val();

    console.log('equipoA:' + equipoA);
    console.log('equipoB:' + equipoB);

    insertPronostico(equipoA, equipoB, consoleError, consoleSuccess);
}


////////////////////////////////////////////////////////////////////////////////

function initDatabase() {
    var dbCreada = localStorage.getItem('dbCreada');

    if (!dbCreada) {
        createTablePronostico(consoleError, function() {
            localStorage.setItem('dbCreada', 1);
        });
    }
}

function getDatabase() {
    return window.openDatabase('pronosticoDB', '1', 'Pronostico', 200*1024);
}

function executeSql(sqlSentence, params, errorCB, successCB) {
    getDatabase().transaction(function(tx) {
        tx.executeSql(sqlSentence, params);
    }, errorCB, successCB);
}

function createTablePronostico(errorCB, successCB) {
    var sqlSentence = 'CREATE TABLE IF NOT EXISTS PRONOSTICO (ID INTEGER PRIMARY KEY AUTOINCREMENT,'
    + ' MARCADORA INT, MARCADORB INT)';
    executeSql(sqlSentence, [], errorCB, successCB);
}

function insertPronostico(equipoA, equipoB, errorCB, successCB) {
    var sqlSentence = 'INSERT INTO PRONOSTICO (MARCADORA, MARCADORB) VALUES (?,?)';
    var params = [equipoA, equipoB];
    executeSql(sqlSentence, params, errorCB, successCB);
}

/////////////////////////////////////////////////////////////////////////////

function consoleSuccess(arg) {
    console.log('Exito.', arg);
}

function consoleError(arg) {
    console.log('Falla.', arg);
}