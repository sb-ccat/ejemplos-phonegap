var exec = cordova.require('cordova/exec');

module.exports = {
	alert : function(str, callback) {
        exec(callback, function(err) {
            callback('Error en el plugin Toast.');
        }, "Toast", "alert", [str]);
    }
};