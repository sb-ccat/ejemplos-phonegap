package com.ccat.phonegap;

import android.content.Context;
import org.apache.cordova.*;
import org.json.*;

public class Toast extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        try {
            if (action.equals("alert")) {
                this.alert(args.getString(0), callbackContext);
                return true;
            }

            callbackContext.error("error: action '" + action + "' is not valid");
            return false;

        } catch (JSONException ex) {
            callbackContext.error("error: '" + ex.getMessage());
            return false;
        } catch (Exception ex) {
            callbackContext.error("error: '" + ex.getMessage());
            return false;
        }
    }

    private void alert(String text, CallbackContext callbackContext) {
        try {
            Context context = this.cordova.getActivity().getApplicationContext();

            android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT).show();

            callbackContext.success("1");
        } catch (Exception ex) {
            callbackContext.success("0");
        }
    }
}
