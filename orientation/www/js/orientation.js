var watchID;

var orientationOptions = { frequency: 100 };

//var orientationOptions = { filter: 1 };

function getHeading() {
    console.log('getHeading');
    navigator.compass.getCurrentHeading(respuesta, errorCB, orientationOptions);
}

function respuesta(heading) {
    console.log('respuesta');

    $('#magheading').text(heading.magneticHeading);
    $('#trueheading').text(heading.trueHeading);
    $('#accuracy').text(heading.headingAccuracy);
    $('#timestamp').text(new Date(heading.timestamp).toLocaleString());
}

function toggleWatchHeading() {
    console.log('watchHeading');

    if (watchID) {
        $('#watchHeading').text('Iniciar Rastreo');
        navigator.compass.clearWatch(watchID);
        watchID = undefined;
    } else {
        $('#watchHeading').text('Detener Rastreo');
        watchID = navigator.compass.watchHeading(respuesta, errorCB, orientationOptions);
    }
}

function errorCB(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            alert('Permiso denegado.\n' + error.message);
            break;
        case error.POSITION_UNAVAILABLE:
            alert('Posición no disponible.\n' + error.message);
            break;
        case error.TIMEOUT:
            alert('Tiempo de espera agotado.\n' + error.message);
            break;
    }
}