console.log('iniciando...');

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    console.log('device is ready');

    $('#alert').click(dialogsAlert);
    $('#confirm').click(dialogsConfirm);   
    $('#prompt').click(dialogsPrompt);   
    $('#beep').click(dialogsBeep);   
    $('#toastAlert').click(toastAlert);

    document.addEventListener('backbutton', navigator.app.exitApp, false);
}

/////

function dialogsAlertCB() {
	console.log('navigator.notification.alert completado');
}

function dialogsAlert() {
	navigator.notification.alert('Este es el \n navigator.notification.alert', 
		dialogsAlertCB, 'Titulo Alert', 'Cerrar');
}

/////

function dialogsConfirmCB(buttonIndex) {
	console.log('navigator.notification.confirm completado con opción: ' + buttonIndex);
}

function dialogsConfirm() {
	navigator.notification.confirm('Este es el \n navigator.notification.confirm', 
		dialogsConfirmCB, 'Titulo Confirm', ['Opcion 1', 'Opcion 2', 'Opcion 3']);
}

/////

function dialogsPromptCB(result) {
	console.log('navigator.notification.prompt completado', result);
}

function dialogsPrompt() {
	navigator.notification.prompt('Este es el \n navigator.notification.prompt', 
		dialogsPromptCB, 'Titulo Prompt', ['Opcion 1', 'Opcion 2', 'Opcion 3'], 'Ingrese su edad');
}

/////

function dialogsBeep() {
	navigator.notification.beep(3);
}

/////

function toastAlertCB() {
	console.log('navigator.toast.alert completado');
}

function toastAlert() {
	navigator.toast.alert('Este es el \n navigator.toast.alert', toastAlertCB);
}