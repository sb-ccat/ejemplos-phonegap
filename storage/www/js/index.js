console.log('iniciando...');

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    console.log('device is ready');

    document.addEventListener("backbutton", salir, false);

    webStoragePageIniciado();
}

function salir() {
	console.log('evento backbutton');
	navigator.app.exitApp();
}

function webStoragePageIniciado() {
    $('#toLocal').click(function(event) {
        var value = $('#valueToStore').val();
        localStorage.setItem('storedValue', value);
    });

    $('#toSession').click(function(event) {
        var value = $('#valueToStore').val();
        sessionStorage.setItem('storedValue', value);
    });

    $('#fromLocal').click(function(event) {
        var value = localStorage.getItem('storedValue');
        alert(value);
    });

    $('#fromSession').click(function(event) {
        var value = sessionStorage.getItem('storedValue');
        alert(value);
    });
}