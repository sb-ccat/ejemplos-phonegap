var geolocationOptions = { 
    maximumAge: 30000, 
    timeout: 60000, 
    enableHighAccuracy: true 
};

document.addEventListener('deviceready', onDeviceReady, false);

var map;

function initMap() {
        // Create a map object and specify the DOM element for display.
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          scrollwheel: false,
          zoom: 8
        });
    } 

function onDeviceReady() {
    console.log('deviceready');
    navigator.geolocation.getCurrentPosition(respuesta, console.log, geolocationOptions);
}

function respuesta(position) {
    console.log('respuesta - Latitud, Longitud: ' + 
        position.coords.latitude + ', ' + 
        position.coords.longitude);

    var latLong = {lat: position.coords.latitude, 
        lng: position.coords.longitude};

    map.panTo(latLong); 
    map.setZoom(15);

    var marker = new google.maps.Marker({
        position: latLong,
        map: map,
        title: 'Hello World!'
      });
}