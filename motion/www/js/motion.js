var watchID;

var accelerometerOptions = { 
    frequency: 100 
};

function getAcceleration() {
    console.log('getAcceleration');
    navigator.accelerometer.getCurrentAcceleration(respuesta, errorCB, accelerometerOptions);
}

function respuesta(acceleration) {
    console.log('respuesta');

    $('#valX').text(acceleration.x);
    $('#valY').text(acceleration.y);
    $('#valZ').text(acceleration.z);
    $('#timestamp').text(new Date(acceleration.timestamp).toLocaleString());
}

function toggleWatchAcceleration() {
    console.log('watchAcceleration');

    if (watchID) {
        $('#watchAcceleration').text('Iniciar Monitoreo');
        navigator.accelerometer.clearWatch(watchID);
        watchID = undefined;
    } else {
        $('#watchAcceleration').text('Detener Monitoreo');
        watchID = navigator.accelerometer.watchAcceleration(respuesta, errorCB, accelerometerOptions);
    }
}

function errorCB(error) {
    console.log('Error', error);
    alert('Error al leer el acelerómetro.');
}