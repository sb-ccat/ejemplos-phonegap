var watchID;

var geolocationOptions = { 
    maximumAge: 30000, 
    timeout: 60000, 
    enableHighAccuracy: true 
};

function getPosition() {
    console.log('getPosition');
    navigator.geolocation.getCurrentPosition(respuesta, errorCB, geolocationOptions);
}

function respuesta(position) {
    console.log('respuesta - Latitud, Longitud: ' + 
        position.coords.latitude + ', ' + 
        position.coords.longitude);

    $('#latitude').text(position.coords.latitude);
    $('#longitude').text(position.coords.longitude);
    $('#altitude').text(position.coords.altitude);
    $('#accuracy').text(position.coords.accuracy);
    $('#altitudeAccuracy').text(position.coords.altitudeAccuracy);
    $('#heading').text(position.coords.heading);
    $('#speed').text(position.coords.speed);
    $('#timestamp').text(new Date(position.timestamp).toLocaleString());
}

function toggleWatchPosition() {
    console.log('watchPosition');

    if (watchID) {
        $('#watchPosition').text('Iniciar Rastreo');
        navigator.geolocation.clearWatch(watchID);
        watchID = undefined;
    } else {
        $('#watchPosition').text('Detener Rastreo');
        watchID = navigator.geolocation.watchPosition(respuesta, errorCB, geolocationOptions);
    }
}

function errorCB(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            alert('Permiso denegado.\n' + error.message);
            break;
        case error.POSITION_UNAVAILABLE:
            alert('Posición no disponible.\n' + error.message);
            break;
        case error.TIMEOUT:
            alert('Tiempo de espera agotado.\n' + error.message);
            break;
    }
}