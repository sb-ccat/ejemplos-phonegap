function loadDeviceData() {
	$('#cordova').text(device.cordova);
	$('#model').text(device.model);
	$('#platform').text(device.platform);
	$('#uuid').text(device.uuid);
	$('#version').text(device.version);
	$('#manufacturer').text(device.manufacturer);
	$('#isVirtual').text(device.isVirtual);
	$('#serial').text(device.serial);
}