#!/usr/bin/env node
//console.log('sb-specific-platform [START]');

var removeMatchingCodeLines = function(tagName, file) {
	var regex = new RegExp('<' + tagName + '>((.|[\r\n])*?)</' + tagName + '>', 'gim');
	
	var fs = require('fs')
	fs.readFile(file, 'utf8', function (err,data) {
	  if (err) {
		//return console.log(err);
		var msg = err.code;
		msg += '\n';
		msg += err.message;
		msg += '\n';
		msg += err;
		console.log(msg);
	  	return;
	  }
	  //console.log(file);
	  
	  var newData = data.replace(regex, 'removed ' + tagName + ' lines');
	  //console.log(newData);
	  
	  fs.writeFile(file, newData, function (err) {
		  if (err) return console.log(err);
		  //console.log('processed ' + file);
	  });
	});
};

var hasValidExtension = function(exts, filename) {
	for(var i=exts.length; i--;) {
		if (filename.indexOf('.' + exts[i]) >= 0) {
			return true;
		}
	}
	return false;
};

var fs = require('fs');
var walk = function(dir, exts, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);
    var i = 0;
    (function next() {
      var file = list[i++];
      if (!file) return done(null, results);
      file = dir + '/' + file;
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, exts, function(err, res) {
            results = results.concat(res);
            next();
          });
        } else {
          if (hasValidExtension(exts, file)) {
            results.push(file);
          }
          next();
        }
      });
    })();
  });
};

var removeUnmatchingPlatformsCode = function(platforms, platformName, exts) {
	walk(platformsRoot[platformName], exts, function(err, results) {
	    if (err) {
                console.log(err);
	    } else {
		console.log(results);
		
		results.forEach(function(file) {
			platforms.forEach(function(pName) {;
				if (platformName != pName) {
					var tagName = 'sb-platform-specific-' + pName;
					//var tagName = 'sb-specific-platform-android';
					removeMatchingCodeLines(tagName, file);
				}
			});
		});
		
		//console.log("sb-specific-platform [END]");
            }
	});
};



//var platforms = process.env.CORDOVA_PLATFORMS.split(',');
var platforms = 'ios,android'.split(',');

var platformsRoot = [];
platformsRoot['ios'] = 'platforms/ios/www';
platformsRoot['android'] = 'platforms/android/assets/www';

var validExts = ['js', 'html', 'xml', 'css'];

platforms.forEach(function(platformName) {
	removeUnmatchingPlatformsCode(platforms, platformName, validExts);
});

//removeUnmatchingPlatformsCode(platforms, 'ios/www/js/utils', validExts);
