package com.ccat.phonegap;

import lombok.*;

@Getter @Setter @ToString
public class GcmData {
	private String serverApiKey;
	private String deviceToken;
}