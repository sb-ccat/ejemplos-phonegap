package com.ccat.phonegap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksResourceConfiguration {
	public static void main(String[] args) {
		SpringApplication.run(BooksResourceConfiguration.class, args);
	}
}