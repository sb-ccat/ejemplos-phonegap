package com.ccat.phonegap;

import lombok.*;

@Getter @Setter
public class Book {
	private int id;
	private String name;
	private double price;

	public Book(int id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
}