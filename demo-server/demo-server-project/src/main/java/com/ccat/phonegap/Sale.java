package com.ccat.phonegap;

import lombok.*;

@Getter @Setter @ToString
public class Sale {
	private int id;
	private int bookId;
	private String clientName;
	private String date;
}