package com.ccat.phonegap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.*;
import org.springframework.http.*;

@Slf4j
@Controller
@RequestMapping("/books")
public class BooksResource {
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody Book[] getBooks() {
		Book[] books = {
			new Book(1,"La Carta Esférica",250.0),
			new Book(2,"Las Tablas de Flandes",250.0),
			new Book(3,"La Reina del Sur",150.0),
			new Book(4,"La Piel del Tambor",100.0)
		};
		return books;
	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Integer> update(@RequestBody Sale sale) {

	    if (sale != null) {
	        log.info("Venta recibida:\n" + sale.toString());
	    } else {
	    	log.info("No se recibió ninguna venta");
	    }

	    return new ResponseEntity<Integer>(new Integer(1), HttpStatus.OK);
	}

	@RequestMapping(value="/push", method=RequestMethod.POST)
	public ResponseEntity<Integer> push(@RequestBody GcmData data) {

	    if (data != null) {
	        log.info("Preparando notificación:\n" + data.toString());

	        String payload = 
	        "{\"data\":{\"title\":\"Push Demo\",\"text\":\"Mensaje de la notificación\"," +
	        "\"extra\":{\"url\":\"someurl.js\"}},\"to\":\"" + data.getDeviceToken() + "\"}";

	        log.info("Payload is: " + payload);

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "key=" + data.getServerApiKey());
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(payload, headers);

	        RestTemplate restTemplate = new RestTemplate();
			String url = "https://gcm-http.googleapis.com/gcm/send";
			String response = (String) restTemplate.postForObject(url, entity, String.class);

			log.info("Server response is: " + response);
	    } else {
	    	log.info("No se recibió ningún dato");
	    }

	    return new ResponseEntity<Integer>(new Integer(1), HttpStatus.OK);
	}
}