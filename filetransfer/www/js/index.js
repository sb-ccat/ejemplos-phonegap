console.log('iniciando...');

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    console.log('device is ready');


    $('#download').click(downloadFile);
    $('#open').click(openFile);   
    $('#delete').click(deleteFile);

    document.addEventListener('backbutton', navigator.app.exitApp, false);
}

function downloadFile() {
	var fileTransfer = new FileTransfer();
	var uri = encodeURI("http://cdn.mos.cms.futurecdn.net/8a1813ebb675372564938ce7419479a3.jpg");

	fileTransfer.download(
	    uri,
	    cordova.file.externalDataDirectory + '/rioLogo.jpg',
	    function(entry) {
	        console.log("download complete: " + entry.toURL());
	    },
	    function(error) {
	        console.log("could not download, check the console", error);
	    },
	    false,
	    {
	        headers: {
	            //"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
	        }
	    }
	);
}

function openFile() {
	cordova.InAppBrowser.open(cordova.file.externalDataDirectory + 'rioLogo.jpg', '_blank');
}

function deleteFile() {
	window.resolveLocalFileSystemURL(
		cordova.file.externalDataDirectory, 
		deleteFileInDirectory,
		console.log);
}

function deleteFileInDirectory(dirEntry) {
	dirEntry.getFile('rioLogo.jpg', 
		{create: false}, 
		deleteFileEntry, 
	  	console.log);
}

function deleteFileEntry(fileEntry) {
	fileEntry.remove(function() {
			console.log('File removed.');
	   	}, console.log);
}